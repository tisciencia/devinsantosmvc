﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevInSantosMVC.Domain.Model.Entities;
using DevInSantosMVC.Domain.Model.Interfaces;

namespace DevInSantosMVC.Site.Controllers
{
    public class BaseController : Controller
    {
        private readonly IUsers Users;

        public BaseController()
            : this(DependencyResolver.Current.GetService<IUsers>())
        { }

        public BaseController(IUsers users)
        {
            Users = users;
        }

        protected ActionResult ExtendedJson(object result)
        {
            return Json(new { result = result }, JsonRequestBehavior.AllowGet);
        }

        public User CurrentUser
        {
            get {
                // TODO: Buscar o usuario logado do cache
                return (Request.IsAuthenticated) ? Users.FindByUserName(User.Identity.Name) : null;
            }
        }
    }
}

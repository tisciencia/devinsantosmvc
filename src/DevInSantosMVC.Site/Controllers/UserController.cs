﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using DevInSantosMVC.Domain.Model.Entities;
using DevInSantosMVC.Domain.Model.Interfaces;
using DevInSantosMVC.Site.Src.Filters;
using DevInSantosMVC.Site.Models;

namespace DevInSantosMVC.Site.Controllers
{
    public class UserController : BaseController
    {
        public IQuestions Questions { get; set; }
        public IUsers Users { get; set; }

        public UserController()
            : this(DependencyResolver.Current.GetService<IUsers>(),
            DependencyResolver.Current.GetService<IQuestions>())
        {
        }

        public UserController(IUsers users, IQuestions questions)
        {
            Users = users;
            Questions = questions;
        }
        
        [NullFilter("user", "Error")]
        public ViewResult UserProfile(User user)
        {   
            return View("Profile", user);
        }

        [HttpPost]
        [NullFilter("user", "Error")]
        public ActionResult Ask(User user, string question)
        {
            var newQuestion = new Question(CurrentUser, user, question);
            Questions.Add(newQuestion);
            return ExtendedJson(new { success = true, message = "Pergunta enviada com sucesso!" });
        }

        [Authorize]
        [NullFilter("user", "Error")]
        public ActionResult InBox(User user) {
            return View("Inbox", user);
        }

        [HttpPost]
        [NullFilter("question", "Error")]
        public ActionResult Answer(Question question, string answer)
        {
            question.Answer = answer;
            Questions.Update(question);
            return ExtendedJson(new { success = true, message = "Pergunta respondida com sucesso!" });
        }

        [HttpPost]
        [NullFilter("question", "Error")]
        public ActionResult Remove(Question question) {
            Questions.Remove(question);
            return ExtendedJson(new { success = true, message = "Pergunta removida com sucesso!" });
        }

        public ActionResult ListAllUsers() {
            var users = Users.FindAll().ToList();
            return View(users);
        }
    }
}

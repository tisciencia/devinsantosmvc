﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevInSantosMVC.Domain.Model.Interfaces;
using DevInSantosMVC.Site.Src.Extension;

namespace DevInSantosMVC.Site.Controllers
{
    public class RedirectController : Controller
    {
        public ActionResult UserRedirect(string id)
        {
            var Users = DependencyResolver.Current.GetService<IUsers>();
            var user = Users.FindById(id);
            return (user != null) 
                ? new RedirectResult(Url.ForUser(user), true) 
                : new RedirectResult("Error");
        }
    }
}

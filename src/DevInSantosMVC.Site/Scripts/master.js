﻿
var fashionme = window.fashionme || {};

fashionme.closet = new function () {

    this.init = function () {
        this.bindEvents();
    };

    this.bindEvents = function () {
        $("form.ask-form").live('submit', this.ask);
        $("form.answer-form").live('submit', this.answer);
        $(".question .remove").live('click', this.removeQuestion);
    };

    this.ask = function (e) {
        var $this = $(this);
        var ask = $this.find("textarea");

        if (ask.val().length == 0) {
            alert("Invalid question!")
            return false;
        }

        var jqXHR = $.ajax({
            type: 'POST',
            url: $this.attr('action'),
            data: $this.getParams(),
            context: $this,
            doNotRetry: true
        });

        var $submit = $this.find('input[type="submit"], button');
        $submit.data('html', $submit.html());
        $submit.html('Sending...');

        jqXHR.done(function (data) {
            var $this = $(this);
            var $submit = $this.find('input[type="submit"], button');

            $submit.html($submit.data('html'));
            $submit.removeData('html');

            if (data.result.success) {
                alert(data.result.message);
            }
        });
        jqXHR.fail(function () {
            alert("Ajax failure");
        });

        e.preventDefault();
    };

    this.answer = function (e) {
        var $this = $(this);
        var answer = $this.find("textarea");

        if (answer.val().length == 0) {
            alert("Invalid answer!")
            return false;
        }

        var jqXHR = $.ajax({
            type: 'POST',
            url: $this.attr('action'),
            data: $this.getParams(),
            context: $this,
            doNotRetry: true
        });

        var $submit = $this.find('input[type="submit"], button');
        $submit.data('html', $submit.html());
        $submit.html('Sending...');

        jqXHR.done(function (data) {
            var $this = $(this);
            var $submit = $this.find('input[type="submit"], button');

            $submit.html($submit.data('html'));
            $submit.removeData('html');

            $this.parents(".question").remove();

            if (data.result.success) {
                alert(data.result.message);
            }
        });
        jqXHR.fail(function () {
            alert("Ajax failure");
        });

        e.preventDefault();
    };

    this.removeQuestion = function (e) {
        var $this = $(this);

        if (!confirm("Are you sure you want to delete this question?")) {
            return false;
        }

        var jqXHR = $.ajax({
            type: 'POST',
            url: $this.attr('href'),
            context: $this,
            doNotRetry: true
        });

        jqXHR.done(function (data) {
            var $this = $(this);
            $this.parents(".question").remove();
        });
        jqXHR.fail(function () {
            alert("Ajax failure");
        });

        e.preventDefault();
    };
};

$(function () {
    fashionme.closet.init();
});

$.fn.extend({
    getParams: function () {
        var params = {};
        var inputs = $(this).find('input, textarea, select');
        var input;
        for (var i = 0; i < inputs.length; i++) {
            input = $(inputs[i]);
            if (input.attr('name') && input.attr('name') != '') {
                if (input.attr('type') == 'checkbox' && !input.attr('checked')) {
                    continue;
                }
                if (input.attr('type') == 'radio' && !input.attr('checked')) {
                    continue;
                }
                params[input.attr('name')] = input.val();
            }
        }
        return params;
    }
});

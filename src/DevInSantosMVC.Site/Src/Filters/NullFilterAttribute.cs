﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DevInSantosMVC.Site.Src.Filters
{
    public class NullFilterAttribute : ActionFilterAttribute
    {
        private string Parameter;
        private string ViewName;

        public NullFilterAttribute(string parameter)
        {
            Parameter = parameter;
        }

        public NullFilterAttribute(string parameter, string viewName)
        {
            Parameter = parameter;
            ViewName = viewName;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ActionResult result;

            if (filterContext.ActionParameters[Parameter] == null)
            {
                if (String.IsNullOrEmpty(ViewName))
                {
                    result = new EmptyResult();
                }
                else
                {
                    ViewResult view = new ViewResult();
                    view.ViewName = ViewName;
                    result = view;
                }
                filterContext.HttpContext.Response.StatusCode = 404;
                filterContext.Result = result;
            }
            else
            {
                base.OnActionExecuting(filterContext);
            }
        }
    }
}
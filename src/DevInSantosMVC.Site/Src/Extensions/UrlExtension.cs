﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using DevInSantosMVC.Domain.Infra.Helpers;
using DevInSantosMVC.Domain.Model.Entities;
using DevInSantosMVC.Domain.Model.Interfaces;

namespace DevInSantosMVC.Site.Src.Extension
{
    public static class UrlExtension
    {
        public static string ForUser(this UrlHelper urlHelper, string userName, string action = null, object @params = null)
        {
            IUsers Users = DependencyResolver.Current.GetService<IUsers>();
            var user = Users.FindByUserName(userName);
            return ForUser(urlHelper, user, action, @params);
        }

        public static string ForUser(this UrlHelper urlHelper, User user, string action = null, object @params = null)
        {
            RouteValueDictionary dictionary = ToDictionary(@params);
            if (!string.IsNullOrEmpty(action))
            {
                dictionary.Add("action", action);
            }
            else {
                dictionary.Add("action", "UserProfile");
            }
            dictionary["controller"] = "User";
            dictionary["user"] = user.UserName;
            return urlHelper.RouteUrl("user", dictionary);
        }

        private static RouteValueDictionary ToDictionary(object @params)
        {
            RouteValueDictionary dictionary = new RouteValueDictionary();
            if (@params != null)
            {
                if (@params is RouteValueDictionary)
                {
                    return (RouteValueDictionary)@params;
                }
                foreach (PropertyInfo property in ReflectionHelper.GetProperties(@params))
                {
                    dictionary[property.Name] = property.GetValue(@params, null);
                }
            }
            return dictionary;
        }
    }
}
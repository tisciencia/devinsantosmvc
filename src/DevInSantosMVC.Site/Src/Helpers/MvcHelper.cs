﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace DevInSantosMVC.Site.Src.Helpers
{
    public class MvcHelper
    {
        public static List<string> GetActionsFromControllerName(params string[] controllerNames)
        {
            List<string> actions = new List<string>();
            foreach (string controllerName in controllerNames)
            {
                MethodInfo[] methods = GetControllerType(controllerName).GetMethods(BindingFlags.Public | BindingFlags.Instance);

                foreach (MethodInfo method in methods)
                {
                    if ((method.ReturnType == typeof(ActionResult) || method.ReturnType == typeof(ViewResult) || method.ReturnType == typeof(JsonResult) || method.ReturnType == typeof(EmptyResult))
                        && method.GetCustomAttributes(typeof(NonActionAttribute), true).Length == 0)
                    {
                        ActionNameAttribute actionName = (ActionNameAttribute)method.GetCustomAttributes(typeof(ActionNameAttribute), true).FirstOrDefault();
                        actions.Add(actionName != null ? actionName.Name : method.Name);
                    }
                }
            }
            return actions.Distinct().ToList();
        }

        private static Type GetControllerType(string controllerName)
        {
            String defaultNamespace = "DevInSantosMVC.Site.Controllers";
            String suffix = "Controller";
            String typeName = String.Format("{0}.{1}{2}", defaultNamespace, controllerName, suffix);
            return Type.GetType(typeName);
        }
    }
}
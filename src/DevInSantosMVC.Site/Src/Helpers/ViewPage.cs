﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevInSantosMVC.Domain.Model.Entities;
using DevInSantosMVC.Site.Controllers;

namespace DevInSantosMVC.Site.Src.Helpers
{
    public abstract class ViewPage<T> : System.Web.Mvc.WebViewPage<T>
    {
        public User CurrenUser {
            get { 
                return (ViewContext.Controller as BaseController).CurrentUser;
            }
        }
    }
}
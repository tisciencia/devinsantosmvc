﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevInSantosMVC.Domain.Model.Entities;
using DevInSantosMVC.Domain.Model.Interfaces;

namespace DevInSantosMVC.Site.Src.Binders
{
    public class QuestionBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            IQuestions Questions = DependencyResolver.Current.GetService<IQuestions>();
            Question question = null;
            if (bindingContext.ValueProvider.ContainsPrefix(bindingContext.ModelName))
            {
                question = (Question)base.BindModel(controllerContext, bindingContext);

                if (question != null && question.Id != null)
                {
                    Question storedUser = Questions.FindById(question.Id.ToString());

                    if (storedUser != null)
                    {
                        bindingContext.ModelMetadata.Model = storedUser;
                        bindingContext.ValueProvider = new FormValueProvider(controllerContext);
                        question = (Question)base.BindModel(controllerContext, bindingContext);
                    }
                }
                else
                {
                    question = Questions.FindById(bindingContext.ValueProvider.GetValue(bindingContext.ModelName).AttemptedValue);
                }
            }

            return question;
        }
    }
}
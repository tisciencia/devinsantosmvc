﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevInSantosMVC.Domain.Model.Entities;
using DevInSantosMVC.Domain.Model.Interfaces;

namespace DevInSantosMVC.Site.Src.Binders
{
    public class UserBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            IUsers Users = DependencyResolver.Current.GetService<IUsers>();
            User user = null;
            if (bindingContext.ValueProvider.ContainsPrefix(bindingContext.ModelName))
            {
                user = (User)base.BindModel(controllerContext, bindingContext);

                if (user != null && user.Id != null)
                {
                    User storedUser = Users.FindByUserName(user.UserName);

                    if (storedUser != null)
                    {
                        bindingContext.ModelMetadata.Model = storedUser;
                        bindingContext.ValueProvider = new FormValueProvider(controllerContext);
                        user = (User)base.BindModel(controllerContext, bindingContext);
                    }
                }
                else
                {
                    user = Users.FindByUserName(bindingContext.ValueProvider.GetValue(bindingContext.ModelName).AttemptedValue);
                }
            }

            return user;
        }
    }
}
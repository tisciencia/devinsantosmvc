﻿// adicionar jquery
// testar metodos javascript (todas as actions)
// testar persistencia (ignored fields, etc)
// fazer layout

var fashionme = window.fashionme || {};

fashionme.closet = new function () {

    this.init = function () {
        this.bindEvents();
    };

    this.bindEvents = function () {
        $("form.ask-form").live('submit', this.ask);
        $("form.answer-form").live('submit', this.answer);
        $(".question .remove").live('click', this.removeQuestion);
    };

    this.ask = function (e) {
        var $this = $(this);
        var ask = $this.find("textarea");

        if (ask.val().length == 0) {
            alert("Invalid question!")
            return false;
        }

        var jqXHR = $.ajax({
            type: 'POST',
            url: $this.attr('action'),
            data: $this.getParams(),
            context: $this,
            doNotRetry: true
        });

        var $submit = $this.find('input[type="submit"], button');
        $submit.data('html', $submit.html());
        $submit.html('Sending...');

        jqXHR.done(function (data) {
            var $this = $(this);
            var $submit = $this.find('input[type="submit"], button');

            $submit.html($submit.data('html'));
            $submit.removeData('html');

            if (data.result.success) {
                alert(data.result.message);
            }
        });
        jqXHR.fail(function () {
            alert("Ajax failure");
        });

        e.preventDefault();
    };

    this.answer = function (e) {
        var $this = $(this);
        var answer = $this.find("textarea");

        if (answer.val().length == 0) {
            alert("Invalid answer!")
            return false;
        }

        var jqXHR = $.ajax({
            type: 'POST',
            url: $this.attr('action'),
            data: $this.getParams(),
            context: $this,
            doNotRetry: true
        });

        var $submit = $this.find('input[type="submit"], button');
        $submit.data('html', $submit.html());
        $submit.html('Sending...');

        jqXHR.done(function (data) {
            var $this = $(this);
            var $submit = $this.find('input[type="submit"], button');

            $submit.html($submit.data('html'));
            $submit.removeData('html');

            $this.parents(".question").remove();

            if (data.result.success) {
                alert(data.result.message);
            }
        });
        jqXHR.fail(function () {
            alert("Ajax failure");
        });

        e.preventDefault();
    };

    this.removeQuestion = function (e) {
        var $this = $(this);

        if (!confirm("Are you sure you want to delete this question?")) {
            return false;
        }

        var jqXHR = $.ajax({
            type: 'POST',
            url: $this.attr('href'),
            context: $this,
            doNotRetry: true
        });

        jqXHR.done(function (data) {
            var $this = $(this);
            $this.parents(".question").remove();
        });
        jqXHR.fail(function () {
            alert("Ajax failure");
        });

        e.preventDefault();
    };
};

$(function () {
    fashionme.closet.init();
});

$.fn.extend({
    hasAttr: function (attr) {
        return typeof ($(this).attr(attr)) != 'undefined';
    },
    hasData: function (data) {
        return typeof ($(this).data(data)) != 'undefined';
    },
    top: function (value) {
        return $(this).cssNumberProperty('top', value);
    },
    marginTop: function (value) {
        return $(this).cssNumberProperty('margin-top', value);
    },
    marginBottom: function (value) {
        return $(this).cssNumberProperty('margin-bottom', value);
    },
    marginLeft: function (value) {
        return $(this).cssNumberProperty('margin-left', value);
    },
    marginRight: function (value) {
        return $(this).cssNumberProperty('margin-right', value);
    },
    cssNumberProperty: function (property, value) {
        if (typeof (value) == 'undefined') {
            if ($(this).css(property)) {
                return new Number($(this).css(property).replace(/^([0-9]*.?[0-9]+).*/, '$1'));
            }
            else {
                return 0;
            }
        }
        else {
            if (value != null) {
                $(this).css(property, value + 'px');
            }
            else {
                $(this).css(property, '');
            }
        }
    },
    getParams: function () {
        var params = {};
        var inputs = $(this).find('input, textarea, select');
        var input;
        for (var i = 0; i < inputs.length; i++) {
            input = $(inputs[i]);
            if (input.attr('name') && input.attr('name') != '') {
                if (input.attr('type') == 'checkbox' && !input.attr('checked')) {
                    continue;
                }
                if (input.attr('type') == 'radio' && !input.attr('checked')) {
                    continue;
                }
                params[input.attr('name')] = input.val();
            }
        }
        return params;
    },
    loader: function (size) {
        var loader;

        if (typeof (size) == 'undefined' || size == null) {
            size = 'small';
        }
        loader = $('<img src="/static/images/icons/ajax-' + size + '-loader.gif" alt="Loading" />');
        $(this).data('html', $(this).html());
        $(this).html(loader);
        $(this).addClass('loading');
        return $(this);
    },
    endLoader: function () {
        $(this).html($(this).data('html'));
        $(this).data('html', '');
        $(this).removeClass('loading');
        return $(this);
    },
    loadImages: function (func) {
        var self = this;
        images = this.find('img');
        images.each(function (i, e) {
            $(e).css('visibility', 'hidden');
            $(e).bind('load error', function () {
                var img = $(this);
                img.addClass('loaded');
                img.css('visibility', '');
                if (images.length == images.filter('.loaded').length) {
                    func.apply(self, []);
                }
            });
            if (e.complete) {
                $(e).load();
            }
        });
    }
});

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using DevInSantosMVC.Site.Src.Helpers;
using DevInSantosMVC.Domain.Infra.Helpers;

namespace DevInSantosMVC.Site
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("user", "{user}/{action}",
                new { controller = "User", action = "UserProfile" },
                new { user = RegexHelper.USERNAME_PATTERN,
                      action = RegexHelper.ForWords(MvcHelper.GetActionsFromControllerName("User")) });

            routes.MapRoute("redirect-userid", "{id}",
                new { controller = "Redirect", action = "UserRedirect" },
                new { id = RegexHelper.MONGOID_PATTERN });

            routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
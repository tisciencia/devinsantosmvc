﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using DevInSantosMVC.Domain.Model.Entities;
using DevInSantosMVC.Site.Models;
using MongoDB.Bson;

namespace DevInSantosMVC.Site.App_Start
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.CreateMap<RegisterModel, User>()
                .ForMember(x => x.Id, y => y.Ignore());
        }
    }
}
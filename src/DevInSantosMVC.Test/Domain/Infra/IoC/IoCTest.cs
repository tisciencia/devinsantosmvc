﻿using System;
using System.Web.Mvc;
using DevInSantos.Infra.IoC;
using DevInSantosMVC.Domain.Model.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharpTestsEx;

namespace DevInSantosMVC.Test.Domain.Infra.IoC
{
    [TestClass]
    public class IoCTest
    {
        [TestMethod]
        public void Dependency_resolver_test()
        {
            DependencyConfig.RegisterDependencyInjection();
            var users = DependencyResolver.Current.GetService<IUsers>();
            users.Should().Not.Be(null);
        }
    }
}

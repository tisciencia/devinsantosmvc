﻿using System;
using DevInSantosMVC.Domain.Model.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharpTestsEx;

namespace DevInSantosMVC.Test.Domain.Entities
{
    [TestClass]
    public class UserTest
    {
        [TestMethod]
        public void Two_users_should_be_diff()
        {
            User user1 = new User("tisciencia");
            User user2 = new User("vintem");

            user1.Equals(user2).Should().Be(false);
        }
    }
}

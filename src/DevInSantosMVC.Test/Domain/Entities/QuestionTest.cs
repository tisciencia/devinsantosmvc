﻿using System;
using DevInSantosMVC.Domain.Model.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharpTestsEx;

namespace DevInSantosMVC.Test.Domain.Entities
{
    [TestClass]
    public class QuestionTest
    {
        public const int MESSAGE_MAX_LENGTH = 255;

        [TestMethod]
        public void Question_message_should_be_valid()
        {
            Question question = new Question(new User(), "Olá tudo bem?");
            question.Message.Should().Be("Olá tudo bem?");
        }

        [TestMethod]
        public void Question_answer_should_be_answered()
        {
            Question question = new Question(new User(), "Olá tudo bem?");
            question.Answer = "Sim, obrigado.";

            question.IsAnswered.Should().Be(true);
        }

        [TestMethod]
        public void Question_answer_should_be_not_answered()
        {
            Question question = new Question(new User(), "Olá tudo bem?");

            question.IsAnswered.Should().Be(false);
        }

        [TestMethod]
        public void Question_object_should_be_diff()
        {
            Question question1 = new Question(new User(), "Olá tudo bem?");
            Question question2 = new Question(new User(), "Olá tudo bem?");

            question1.Equals(question2).Should().Be(false);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Question_message_should_be_invalid_when_empty()
        {
            Question question = new Question(new User(), "");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Question_message_should_be_invalid_when_to_is_null()
        {
            Question question = new Question(null, "Olá tudo bem?");
        }

        [TestMethod]
        public void Question_should_be_to_user_tiago()
        {
            User tiago = new User("Tiago");
            Question question = new Question(null, tiago, "Olá tudo bem?");
            question.To.Should().Be(tiago);
        }

        [TestMethod]
        public void Question_should_be_from_user_vintem_to_any_user()
        {
            User tiago = new User("Tiago");
            User vintem = new User("Vintem");
            Question question = new Question(vintem, tiago, "Olá tudo bem?");
            question.From.Should().Be(vintem);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Question_message_should_be_invalid_when_null()
        {
            Question question = new Question(new User(), null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Question_message_should_be_invalid_with_message_256chars()
        {
            char[] charArrayOverMaxLength = new char[MESSAGE_MAX_LENGTH + 1];
            for (int i = 0; i < MESSAGE_MAX_LENGTH + 1; i++)
                charArrayOverMaxLength[i] = '.';

            string messageOverMaxLength = new string(charArrayOverMaxLength);
            Question question = new Question(new User(), messageOverMaxLength);
        }

        [TestMethod]
        public void Question_message_should_be_valid_with_message_255char()
        {
            char[] charArrayMaxLength = new char[MESSAGE_MAX_LENGTH];
            for (int i = 0; i < MESSAGE_MAX_LENGTH; i++)
                charArrayMaxLength[i] = '.';

            string messageMaxLength = new string(charArrayMaxLength);
            Question question = new Question(new User(), messageMaxLength);

            question.Message.Length.Should().Be(MESSAGE_MAX_LENGTH);
        }

        [TestMethod]
        public void Question_should_be_anonym()
        {
            User tiago = new User("Tiago");
            Question question = new Question(null, tiago, "Olá tudo bem?");
            question.IsAnonymous.Should().Be(true);
        }

        [TestMethod]
        public void Question_should_not_be_anonym()
        {
            User tiago = new User("Tiago");
            User vintem = new User("Vintem");
            Question question = new Question(vintem, tiago, "Olá tudo bem?");
            question.IsAnonymous.Should().Be(false);
        }
    }
}

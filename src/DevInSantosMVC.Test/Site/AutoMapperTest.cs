﻿using System;
using AutoMapper;
using DevInSantosMVC.Domain.Model.Entities;
using DevInSantosMVC.Site.App_Start;
using DevInSantosMVC.Site.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharpTestsEx;

namespace DevInSantosMVC.Test.Site
{
    [TestClass]
    public class AutoMapperTest
    {
        [TestMethod]
        public void Verifica_se_configuracao_do_automapper_esta_correta()
        {
            //AutoMapperConfiguration.Configure();
            //Mapper.AssertConfigurationIsValid();
        }

        [TestMethod]
        public void Test_mapeament()
        {
            AutoMapperConfiguration.Configure();
            var model = new RegisterModel
            {
                ConfirmPassword = "123",
                Password = "123",
                Email = "tisciencia@gmail.com",
                UserName = "tisciencia"
            };
            var user = Mapper.Map<RegisterModel, User>(model);
            user.Should().Not.Be(null);
        }

    }
}

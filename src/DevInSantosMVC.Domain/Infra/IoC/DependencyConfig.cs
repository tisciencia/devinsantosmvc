﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using DevInSantosMVC.Domain.Infra.Services;
using DevInSantosMVC.Domain.Model.Interfaces;
using DevInSantosMVC.Domain.Repository.Repositories;
using Microsoft.Practices.Unity;

namespace DevInSantos.Infra.IoC
{
    public class DependencyConfig
    {
        public static void RegisterDependencyInjection()
        {
            IUnityContainer container = new UnityContainer();
            container.RegisterType<IPasswordHashProvider, ShaPasswordHashProvider>();
            container.RegisterType<IUsers, Users>();
            container.RegisterType<IQuestions, Questions>();
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}

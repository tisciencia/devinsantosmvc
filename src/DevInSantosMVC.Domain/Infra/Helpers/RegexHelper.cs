﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevInSantosMVC.Domain.Infra.Helpers
{
    public static class RegexHelper
    {
        public const string USERNAME_PATTERN = @"^[a-zA-Z]+\w+";
        public const string MONGOID_PATTERN = @"^[a-zA-Z0-9]+$";

        public static string ForWords(List<string> words) {
            return string.Format("({0})", string.Join("|", words));
        }
    }
}

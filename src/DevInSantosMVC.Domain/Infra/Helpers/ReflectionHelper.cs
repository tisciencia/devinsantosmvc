﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DevInSantosMVC.Domain.Infra.Helpers
{
    public static class ReflectionHelper
    {
        public static T GetPropertyValue<T>(object o, string propertyName)
        {
            PropertyInfo property = null;
            foreach (PropertyInfo p in o.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                if (p.Name == propertyName)
                {
                    property = p;
                }
            }
            return property != null ? (T)GetPropertyValue(o, property) : default(T);
        }

        public static PropertyInfo[] GetProperties(object o)
        {
            return GetProperties(o, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
        }

        public static PropertyInfo[] GetProperties(object o, BindingFlags flags)
        {
            return o.GetType().GetProperties(flags);
        }

        public static object GetPropertyValue(object o, PropertyInfo propertyInfo)
        {
            return propertyInfo.GetValue(o, null);
        }
    }
}

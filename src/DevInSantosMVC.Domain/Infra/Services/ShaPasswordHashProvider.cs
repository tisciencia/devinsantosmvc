﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using DevInSantosMVC.Domain.Model.Interfaces;

namespace DevInSantosMVC.Domain.Infra.Services
{
    public class ShaPasswordHashProvider : IPasswordHashProvider
    {
        private readonly Encoding _encoding = Encoding.UTF8;

        public string CreateHash(string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                password = string.Empty;
            }

            byte[] buffer = _encoding.GetBytes(password);
            using (var cryptoTransformSha1 = new SHA1CryptoServiceProvider())
            {
                return BitConverter.ToString(cryptoTransformSha1.ComputeHash(buffer)).Replace("-", string.Empty);
            }
        }
    }
}

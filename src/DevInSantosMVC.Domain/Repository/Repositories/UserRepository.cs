﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevInSantosMVC.Domain.Model.Entities;
using DevInSantosMVC.Domain.Model.Interfaces;
using DevInSantosMVC.Repository;

namespace DevInSantosMVC.Domain.Repository.Repositories
{
    public abstract class UserRepository : MongoRepository<User>, IUsers
    {
        public UserRepository()
            : base()
        {
        }

        public new User Add(User entity)
        {
            return base.Add(entity);
        }

        public new User Update(User entity) {
            return base.Update(entity);
        }

        public new User FindById(string id) {
            return base.FindById(id);
        }

        public new IQueryable<User> FindAll() {
            return base.FindAll();
        }

        public User FindByUserName(string userName) {
            return base.FindSingle(m => m.UserName == userName);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevInSantosMVC.Domain.Model.Entities;
using DevInSantosMVC.Domain.Model.Interfaces;
using DevInSantosMVC.Repository;

namespace DevInSantosMVC.Domain.Repository.Repositories
{
    public class Questions : MongoRepository<Question>, IQuestions
    {
        public Questions()
            : base()
        { }

        public new Question Add(Question entity)
        {
            return base.Add(entity);
        }

        public new void Remove(Question entity) {
            base.Remove(entity);
        }

        public new Question Update(Question entity) {
            return base.Update(entity);
        }

        public new Question FindById(string id) {
            return base.FindById(id);
        }

        public List<Question> FindAnsweredByUser(User user)
        {
            return base.FindAll(m => !string.IsNullOrEmpty(m.Answer) && m.To.UserName == user.UserName).OrderByDescending(m => m.CreatedOn).ToList();
        }

        public List<Question> FindUnansweredByUser(User user)
        {
            return base.FindAll(m => string.IsNullOrEmpty(m.Answer) && m.To.UserName == user.UserName).OrderByDescending(m => m.CreatedOn).ToList();
        }
    }
}

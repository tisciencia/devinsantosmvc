﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using System;
using DevInSantosMVC.Domain.Model.Interfaces;

namespace DevInSantosMVC.Repository
{
    public class MongoRepository<T> where T : class, IEntity
    {
        protected internal MongoCollection<T> collection;

        /// <summary>
        /// Initializes a new instance of the MongoRepository class.
        /// Uses the Default App/Web.Config connectionstrings to fetch the connectionString and Database name.
        /// </summary>
        /// <remarks>Default constructor defaults to "MongoServerSettings" key for connectionstring.</remarks>
        protected MongoRepository()
            : this(MongoHelper.GetDefaultConnectionString())
        {
        }

        /// <summary>
        /// Initializes a new instance of the MongoRepository class.
        /// </summary>
        /// <param name="connectionString">Connectionstring to use for connecting to MongoDB.</param>
        protected MongoRepository(string connectionString)
        {
            this.collection = MongoHelper.GetCollectionFromConnectionString<T>(connectionString);
        }

        /// <summary>
        /// Initializes a new instance of the MongoRepository class.
        /// </summary>
        /// <param name="url">Url to use for connecting to MongoDB.</param>
        protected MongoRepository(MongoUrl url)
        {
            this.collection = MongoHelper.GetCollectionFromUrl<T>(url);
        }

        protected virtual T Add(T entity)
        {
            collection.Insert<T>(entity);
            return entity;
        }

        protected virtual void Add(IEnumerable<T> entities)
        {
            collection.InsertBatch<T>(entities);
        }

        protected virtual T Update(T entity)
        {
            collection.Save<T>(entity);
            return entity;
        }

        protected virtual void Update(IEnumerable<T> entities)
        {
            foreach (T entity in entities)
            {
                collection.Save<T>(entity);
            }
        }

        protected virtual void Save(T entity)
        {
            throw new NotImplementedException();
        }

        protected virtual void Save(IEnumerable<T> entities)
        {
            throw new NotImplementedException();
        }

        protected virtual void RemoveAll()
        {
            collection.RemoveAll();
        }

        protected virtual void Remove(T entity)
        {
            Remove(entity.Id);
        }

        protected virtual void Remove(string id)
        {
            Remove(MongoHelper.ConvertStringToObjectId(id));
        }

        protected virtual void Remove(ObjectId id)
        {
            collection.Remove(Query.EQ("_id", id));
        }

        protected virtual void Remove(Expression<Func<T, bool>> criteria)
        {
            foreach (T entity in collection.AsQueryable<T>().Where(criteria))
            {
                Remove(entity.Id);
            }
        }

        protected virtual T FindById(string id)
        {
            return FindById(MongoHelper.ConvertStringToObjectId(id));
        }

        protected virtual T FindById(MongoDB.Bson.ObjectId id)
        {
            return collection.FindOneByIdAs<T>(id);
        }

        protected virtual T FindSingle(Expression<Func<T, bool>> criteria)
        {
            return collection.AsQueryable<T>().Where(criteria).FirstOrDefault();
        }

        protected virtual IQueryable<T> FindAll()
        {
            return collection.AsQueryable<T>();
        }

        protected virtual IQueryable<T> FindAll(Expression<Func<T, bool>> criteria)
        {
            return collection.AsQueryable<T>().Where(criteria);
        }

        protected virtual IQueryable<T> FindAll(int page, int pageSize)
        {
            return ColletionHelper.Page(FindAll(), page, pageSize);
        }

        public virtual long Count()
        {
            return collection.Count();
        }

        public virtual IDisposable RequestStart()
        {
            return RequestStart(false);
        }

        public virtual IDisposable RequestStart(bool slaveOk)
        {
            return collection.Database.RequestStart(slaveOk);
        }

        public virtual void RequestDone()
        {
            collection.Database.RequestDone();
        }
    }
}

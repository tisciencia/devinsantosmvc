﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevInSantosMVC.Domain.Model.Attributes;
using DevInSantosMVC.Domain.Model.Interfaces;
using MongoDB.Bson;

namespace DevInSantosMVC.Domain.Model.Entities
{
    [CollectionName("questions")]
    public class Question : IEntity
    {
        private const int MESSAGE_MAX_LENGTH = 255;

        public ObjectId Id { get; set; }
        public string Message { get; private set; }
        public string Answer { get; set; }
        public User From { get; private set; }
        public User To { get; private set; }
        public DateTime CreatedOn { get; private set; }

        public bool IsAnswered
        {
            get { return !String.IsNullOrEmpty(Answer); }
        }

        public bool IsAnonymous
        {
            get { return (From == null); }
        }

        public Question(User from, User to, string message)
        {
            if (to == null)
                throw new ArgumentException("The parameter 'User to' can not be null");
            if (!IsMessageValid(message))
                throw new ArgumentException("Iinvalid message");

            CreatedOn = DateTime.Now;
            To = to;
            From = from;
            Message = message;
        }

        public Question(User to, string message)
            : this(null, to, message)
        {
        }

        public bool IsMessageValid(string message)
        {
            return (!(String.IsNullOrEmpty(message)) && (message.Trim().Length <= MESSAGE_MAX_LENGTH));
        }
    }
}

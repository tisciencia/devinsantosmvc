﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml.Serialization;
using DevInSantosMVC.Domain.Model.Attributes;
using DevInSantosMVC.Domain.Model.Interfaces;
using DevInSantosMVC.Domain.Repository.Repositories;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DevInSantosMVC.Domain.Model.Entities
{
    [CollectionName("users")]
    public class User : UserRepository, IEntity
    {
        public ObjectId Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        [BsonIgnore]
        private readonly IQuestions Questions = DependencyResolver.Current.GetService<IQuestions>();
        [BsonIgnore]
        private List<Question> _answeredQuestions = null;
        [BsonIgnore]
        public List<Question> AnsweredQuestions {
            get { 
                if (_answeredQuestions == null){
                    _answeredQuestions = Questions.FindAnsweredByUser(this);
                }
                return _answeredQuestions;
            }
        }
        [BsonIgnore]
        private List<Question> _unAnsweredQuestions = null;
        [BsonIgnore]
        public List<Question> UnAnsweredQuestions
        {
            get
            {
                if (_unAnsweredQuestions == null)
                {
                    _unAnsweredQuestions = Questions.FindUnansweredByUser(this);
                }
                return _unAnsweredQuestions;
            }
        }

        public User(){
        }

        public User(string username)
        {
            UserName = username;
        }

        public User(string username, string email, string password) 
        {
            UserName = username;
            Email = email;
            Password = password;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevInSantosMVC.Domain.Model.Interfaces
{
    /// <summary>
    /// Interface for password hash provider
    /// </summary>
    public interface IPasswordHashProvider
    {
        /// <summary>
        /// Creates the hash.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <returns>Hash of the password</returns>
        string CreateHash(string password);
    }
}

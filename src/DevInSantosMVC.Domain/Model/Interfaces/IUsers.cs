﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevInSantosMVC.Domain.Model.Entities;

namespace DevInSantosMVC.Domain.Model.Interfaces
{
    public interface IUsers
    {
        User Add(User entity);
        User Update(User entity);
        User FindByUserName(string userName);
        User FindById(string id);
        IQueryable<User> FindAll();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevInSantosMVC.Domain.Model.Entities;

namespace DevInSantosMVC.Domain.Model.Interfaces
{
    public interface IQuestions
    {
        Question Add(Question entity);
        void Remove(Question entity);
        Question Update(Question entity);
        Question FindById(string id);
        List<Question> FindAnsweredByUser(User user);
        List<Question> FindUnansweredByUser(User user);     
    }
}

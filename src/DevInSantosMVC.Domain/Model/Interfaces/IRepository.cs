﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace DevInSantosMVC.Domain.Model.Interfaces
{
    public interface IRepository<T>
    {
        T Add(T entity);
        void Add(IEnumerable<T> entities);

        T Update(T entity);
        void Update(IEnumerable<T> entities);

        void Save(T entity);
        void Save(IEnumerable<T> entities);

        void RemoveAll();
        void Remove(T entity);
        void Remove(string id);
        void Remove(ObjectId id);
        void Remove(Expression<Func<T, bool>> criteria);

        T GetById(string id);
        T GetById(ObjectId id);
        T GetSingle(Expression<Func<T, bool>> expression);

        IQueryable<T> GetAll();
        IQueryable<T> GetAll(Expression<Func<T, bool>> criteria);
        IQueryable<T> GetAll(int page, int pageSize);

        long Count();
        bool Exists(Expression<Func<T, bool>> criteria);
        IDisposable RequestStart();
        IDisposable RequestStart(bool slaveOk);
        void RequestDone();
    }
}
